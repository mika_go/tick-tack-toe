enum Node {
  None,
  Cross,
  Circle
}

enum Move {
  Cross,
  Circle
}

export {Node, Move};
