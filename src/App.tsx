import React, {Component} from 'react';
import './App.sass';
import {Node, Move} from './types';
import Field from './components/Field'

interface State {
  field: Node[][];
  move: Move;
  winner: string;
  steps: number;
}

class App extends Component<{}, State> {
  size: number = 2;

  constructor(props: any) {
    super(props);

    const field = [];
    for (let i = 0; i < this.size; ++i) {
      const row = [];
      for (let j = 0; j < this.size; ++j) {
        row.push(Node.None)
      }
      field.push(row)
    }

    this.state = {
      field: field,
      move: Move.Cross,
      winner: '',
      steps: this.size * this.size
    };
  }

  handleMove = (place: [number, number]) => {
    const [x, y] = place;
    const {field, move, winner, steps} = this.state;
    if (winner !== '') {
      return;
    }
    field[x][y] = move === Move.Cross ? Node.Cross : Node.Circle;

    if (this.checkWinner(field)) {
      this.setState({winner: move === Move.Cross ? 'Cross' : 'Circle'})
    } else if (steps - 1 === 0) {
      this.setState({winner: "No one"});
    }

    this.setState({field: field, move: move === Move.Cross ? Move.Circle : Move.Cross, steps: steps - 1})
  };

  checkWinner = (field: Node[][]): boolean => {
    for (let row of field) {
      let isWin = true;
      for (let j = 0; j < this.size - 1; ++j) {
        isWin = isWin && row[j] === row[j + 1];
        if (!isWin) break;
      }

      if (isWin && row[0] !== Node.None) {
        return true;
      }
    }

    for (let i = 0; i < this.size; ++i) {
      const col = [];
      for (let j = 0; j < this.size; ++j) {
        col.push(field[j][i])
      }
      let isWin = true;
      for (let j = 0; j < this.size - 1; ++j) {
        isWin = isWin && col[j] === col[j + 1];
        if (!isWin) break;
      }

      if (isWin && col[0] !== Node.None) {
        return true;
      }
    }

    let isWin = true;
    for (let i = 0; i < this.size - 1; ++i) {
      isWin = isWin && field[i][i] === field[i + 1][i + 1];
      if (!isWin) break;
    }
    if (isWin && field[0][0] !== Node.None) {
      return true;
    }

    isWin = true;
    for (let i = 0; i < this.size - 1; ++i) {
      isWin = isWin && field[i][this.size - 1 - i] === field[i + 1][this.size - 2 - i];
      if (!isWin) break;
    }
    return isWin && field[0][this.size - 1] !== Node.None;
  };

  reset = () => {
    const {field} = this.state;
    const newField = field.map((row) => row.map((node) => Node.None));
    this.setState({field: newField, move: Move.Cross, winner: '', steps: this.size * this.size})
  };

  render() {
    const {field, move, winner} = this.state;
    return (
      <div className="app">
        <h2>{winner ? `Winner is "${winner}" player` : ''}</h2>
        {winner.length === 0 && <h3>Player "{move === Move.Cross ? 'Cross' : 'Circle'}" move now</h3>}
        <Field field={field} handleMove={this.handleMove}/>
        <button type="button" onClick={this.reset}>Reset</button>
      </div>
    );
  }
}

export default App;
