import React from 'react';
import {Node} from '../types';
import './Field.sass'

interface Props {
  field: Node[][];
  handleMove: (place: [number, number]) => void;
}

export default (props: Props) => {
  const {field} = props;
  return (
    <table className="field">
      <tbody>
      {
        field.map((row, rowInd) => {
          return <tr key={rowInd}>{row.map((node: Node, nodeInd) => {
            switch (node) {
              case Node.None:
                return <td key={nodeInd} onClick={() => props.handleMove([rowInd, nodeInd])}/>;
              case Node.Cross:
                return <td key={nodeInd}>X</td>;
              case Node.Circle:
                return <td key={nodeInd}>O</td>;
            }
          })}</tr>
        })
      }
      </tbody>
    </table>
  )
}
